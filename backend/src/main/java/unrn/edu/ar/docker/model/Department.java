package unrn.edu.ar.docker.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "department")
public class Department {

	@Id
	private String id;

	@Field("name")
	private String name;

	@Field("employeesId")
	private List<String> employeesId = new ArrayList<>();

	public Department() {

	}

	public Department(String name) {
		super();
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getEmployeesId(){
		return this.employeesId;
	}

	public void setEmployeesId(List<String> employeesId) {
		this.employeesId = employeesId;
	}
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "]";
	}

}
